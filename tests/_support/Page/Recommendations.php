<?php
namespace backend\tests\Page;

class Recommendations
{
    // include url of current page
    public static $URL = "/get_product_product_recommendations";
    public static $Request = [
        "positive" => [
            0 => ["sku" => "LO019EMJGZ27", "limit" => 2],
            1 => ["sku" => "LO019EMJGZ27"],
        ],
        "negative" => [
            0 => ["sku" => "LO019EMJGZ27", "limit" => -1],
            1 => ['sku' => 'LO019EMJGZ27', "limit" => ""],
            2 => ['sku' => 'LO019EMJGZ2', "limit" => 1],
            3 => ['skul' => 'LO019EMJGZ27', "limit" => 1],
        ]
    ];
    public static $Response = [
        'positive' => [
            0 => [["product" => ["sku" => "LO019EMJGZ29"]], ["product" => ["sku" => "FI015EMCUI06"]]],
            1 => '12'
        ],
        'negative' => [
            0 => ["faultcode" => "Client.ValidationError", "faultstring" => "The value '-1' could not be validated.", "detail" => null],
            1 => ["faultcode" => "Client.ValidationError", "faultstring" => "The value \"u''\" could not be validated.", "detail" => null],
            2 => ["faultcode" => "Client.RECOMMENDATIONS_NOT_AVAILABLE", "faultstring" => "Recommendation service is not available", "detail" => null],
            3 => ["faultcode" => "Client.ValidationError", "faultstring" => "\"'sku'\" member must occur at least 1 times.", "detail" => null],
        ]
    ];
    public static $Node = '$..product.sku';
    public static $XSS = "lid=rBIABVoNkygwvQAjBTYxAgA=\'\"()&%<acx><ScRiPt%20>prompt(954285)</ScRiPt>";
    public static $InternalError = ["faultcode" => "Server", "faultstring" => "InternalError", "detail" => null];

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \backend\tests\ApiTester;
     */
    protected $tester;

    public function __construct(\backend\tests\ApiTester $I)
    {
        $this->tester = $I;
    }
}
