<?php
namespace backend\tests\api;

use backend\tests\ApiTester;
use \Codeception\Util\HttpCode;
use backend\tests\Page\Recommendations as PRPage;
/**
 * Class RecommendationsCest
 */
class RecommendationsCest
{
    /**
     * @param ApiTester $I
     * @param $scenario
     */
    public function getTwoRecommendations(ApiTester $I, $scenario)
    {
        $I->wantTo('Get 2 product recommendations');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(PRPage::$URL, PRPage::$Request['positive'][0]);
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(PRPage::$Response['positive'][0]);
    }

    /**
     * @param ApiTester $I
     * @param $scenario
     * Так же данное условие выполняется при limit = 0
     */
    public function getAllRecommendations(ApiTester $I, $scenario)
    {
        $I->wantTo('Get all product recommendations');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(PRPage::$URL, PRPage::$Request['positive'][1]);
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $products = $I->grabDataFromResponseByJsonPath(PRPage::$Node);
        $countProducts = count($products);
        $I->assertTrue(
            $countProducts > PRPage::$Response['positive'][1],
            "there are more than 12 entries in response: \"$countProducts\"."
        );
    }

    /**
     * @param ApiTester $I
     * @param $scenario
     */
    public function getBadRequest(ApiTester $I, $scenario)
    {
        $I->wantTo('Get error (Bad request: 400) due to incorrect params');
        $countRequests = count(PRPage::$Request['negative']);
        for ($i = 0; $i < $countRequests; $i++) {
            $I->haveHttpHeader('Content-Type', 'application/json');
            $I->sendPOST(PRPage::$URL, PRPage::$Request['negative'][$i]);
            $I->seeResponseCodeIs(HttpCode::BAD_REQUEST); // 200
            $I->seeResponseIsJson();
            $I->seeResponseContainsJson(PRPage::$Response['negative'][$i]);
        }
    }

    /**
     * @param ApiTester $I
     * @param $scenario
     */
    public function getLocked(ApiTester $I, $scenario)
    {
        $I->wantTo('Get error (Locked: 423) due to XSS injection into cookie');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Cookie', PRPage::$XSS);
        $I->sendPOST(PRPage::$URL, PRPage::$Request['positive'][0]);
        $I->seeResponseCodeIs(HttpCode::LOCKED); // 423
        $I->seeResponseContains("Error 423");
    }

    /**
     * @param ApiTester $I
     * @param $scenario
     */
    public function checkInternalError(ApiTester $I, $scenario)
    {
        $I->wantTo('check for internal error (500)');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST(PRPage::$URL, PRPage::$Request['positive'][0]);
        $I->dontSeeResponseCodeIs(HttpCode::INTERNAL_SERVER_ERROR); // 500
        $I->dontSeeResponseContainsJson(PRPage::$InternalError);
    }
}